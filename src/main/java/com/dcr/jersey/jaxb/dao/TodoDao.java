package com.dcr.jersey.jaxb.dao;

import java.util.HashMap;
import java.util.Map;

import com.dcr.jersey.jaxb.model.Todo;


public enum TodoDao {
	instance;
	private Map<String,Todo> contentProvider = new HashMap<String, Todo>();
	private TodoDao(){
		Todo todo = new Todo("1","Summary 1 - REST");
		todo.setDescription("Rest todo Description 1");
		contentProvider.put("1",todo);
		todo = new Todo("2","Summary 2 - REST");
		todo.setDescription("Rest todo Description 2 ");
		contentProvider.put("2", todo);
	}
	public Map<String,Todo> getModel(){
		return contentProvider;
	}
}
