package com.dcr.jersey.client;



import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Form;

import org.glassfish.jersey.client.ClientConfig;


import com.dcr.jersey.jaxb.model.Todo; 

public class TodosResourceClient {

	
	public static void main(String[] args) {
		ClientConfig config = new ClientConfig();
	    Client client = ClientBuilder.newClient(config);
	    WebTarget target = client.target(getBaseURI());
	    	
	    Todo todo = new Todo("38","38 - Summary ReEntred");
	    
	    target.path("rest").path("todos").request().post(Entity.json(todo));
	    
	    Response response = target.path("rest").path("todos").path(todo.getId()).request()
	    	    .accept(MediaType.APPLICATION_XML).get(Response.class);
	    
	    System.out.println("\n POST Response Code is : "+ response.getStatus());
	    
	    System.out.println(target.path("rest").path("todos").path(todo.getId()).request()
			    	    .accept(MediaType.APPLICATION_JSON).get(Response.class)
			    	    .toString()+"\n");
			    
	    System.out.println(target.path("rest").path("todos").request()
	    	    		.accept(MediaType.APPLICATION_JSON).get(String.class)+"\n");
			    
	    System.out.println(target.path("rest").path("todos").request()
	    	    		.accept(MediaType.APPLICATION_XML).get(String.class)+"\n");
			 
	    System.out.println(target.path("rest").path("todos/2").request()
	    	    		.accept(MediaType.APPLICATION_JSON).get(String.class)+"\n");
			    
			    
			    //target.path("rest").path("todos/38").request().delete();
			    
			    System.out.println(target.path("rest").path("todos").request()
	    	    		.accept(MediaType.APPLICATION_XML).get(String.class)+"\n");
			   
			    Form form = new Form();
			    form.param("id","45");
			    form.param("summary","Summary for id 45");
			    response = target.path("rest").path("todos").request().post(Entity.form(form));
			    
			    System.out.println("\nPost by FORM response code is "+ response.getStatus()+"\n");
			    
			    response = target.path("rest").path("todos/45").request().delete();// deletes a request
			    
			    System.out.println("\nDelete by FORM response code is "+ response.getStatus()+"\n");
			    
	}
	private static URI getBaseURI() {

	    return UriBuilder.fromUri("http://localhost:8080/com.dcr.jersey.first").build();

	  }

}
